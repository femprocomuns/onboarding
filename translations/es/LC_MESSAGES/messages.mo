��    \      �     �      �     �     �     �                       -   %     S  
   c     n     �     �     �     �     �     �     �     �  
   �     �     �     	     	     	     &	     ,	     :	     G	     P	     V	     ]	  
   e	     p	     u	     �	     �	     �	     �	     �	  3   �	  
   
     )
     :
     C
     H
  
   O
     Z
     k
  &   y
     �
     �
     �
     �
     �
     �
      �
  (   �
  0        P     e  
   v     �     �     �     �     �     �     �     �     �     �               >     Q     l     �  
   �  
   �     �  "   �     �  
   �  	   �               *     2     O     o  �  |               +     <     C     X     _  4   g     �  	   �     �     �     �     �     �     �     
          +     @     O  !   h     �     �     �     �     �     �     �     �     �     �  	   �     �     �            )   4     ^     v  =   �     �     �     �          	               8  5   H     ~     �  	   �     �     �     �     �  6   �  9        ?     X     o     �     �     �     �     �  	     
     
        $     6     N  m   ]  m   �  e   9  �   �  �   r  W    	   i  �   s  :  d     �     �      �%     �%  �   �%     �&    �&  �   �'  �  �(     6   Z   Y       \   0   -   W         K   G      B   8       $               E   3             X   
   V   =                ,      :       2   %   !   1              5       "   S      P           .          D       F   '   A               @           &   /      7   J                  )         Q   T          4                        #   +       (      O       M   <      ?              N   [                  H   R   C   9      I       U         L   *   	       >   ;       (check spam) About us Activate account Avatar Avatar changed OK Body Cancel Cannot delete. User has been exported to LDAP Change password Collective Credentials needed Delete Delete entry Description Edit Edit avatar Email Email address Email confirmed OK Email sent Emails do not match Forgotten your password? From Fullname Groups Hello Image formats Kind regards Language Login Logout Members My profile Name New password Notify manager Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Please be patient. This might take one or two days. Recipients Recover password Registre Save Search Send email Send email again Send email to Send the user an email to set password Service Set password Sign up Subject Support Surname Tell us something about yourself There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone Unable to update Updated OK User deleted User not found User not found: %s Username We've sent you an email Websites Welcome Your groups Your password is Your username is Your websites _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective collective_management_explained collective_manager_group_explained edit index_page is better is not a good password register_page service service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-11-27 19:41+0100
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
X-Generator: Poedit 1.8.11
 (mira el spam) Sobre nosotros Activa la cuenta Avatar Se cambió el avatar Cuerpo Cancela No se puede borrar. El usuario ya se exportó a LDAP Cambia contraseña Colectivo Hacen falta credenciales Borrar Borra la entrada Descripción Edita Edita avatar Correo-e Dirección del correo-e Correo confirmado OK Correo enviado No coinciden los correos ¿Te has olvidado la contraseña? De Nombre completo Grupos Hola Formatos de imagen Cordiales saludos Idioma Entrar Salir Miembros Mi perfil Nombre Contraseña nueva Notificar gestor Onboarding terminado No se ha terminado el onboarding todavía Contraseña cambiado OK Contraseñas deben coincidir Por favor, ten paciencia. Esto podría tardar un o dos días. Destinatarias Recuperar la contraseña Registra Guardar Buscar Enviar correo Envía el correo de nuevo Enviar correo a Envía un correo al usuario para fijar la contraseña Servicio Fija la contraseña Apúntate Asunto Soporte Apellido Dinos algo de ti Hace dos veranos había pulgas en el hotel Menorquina. Piensa en una contraseña más larga y fácil de recordar Eso no se puede deshacer No se puede actualizar Actualizado correctamente Se ha borrado el usuario No se encuentra el usuario Usuario no encontrado: %s Nombre de usuario Te hemos enviado un correo Sitio web Bienvenido Tus grupos Tu contraseña es Tu nombre de usuario es Tus sitios web Bienvenido a commonsCloud.

Por favor, confirma tu dirección de correo en este enlace
%s

Cordiales saludos. Bienvenido a commonsCloud.

Por favor, confirma tu dirección de correo en este enlace
%s

Cordiales saludos. Hola,

Una persona está esperando que le des de alta en %s.

Nombre: %s
email: %s

%s

Kind regards. Hola %s,

Alguien a pedido que se cambie tu contraseña.
Si no ha sido tu, por favor ignora este correo.

Tu nombre de usuario es: %s

Puedes cambiar tu contraseña del commonsCloud aquí
%s

Cordiales saludos. Hola %s,

Se ha creado tu cuenta en commonsCloud.
Tu nombre de usuario es: %s (Recuérdalo!)

Por favor, establece tu contraseña aquí
%s

Cordiales saludos. <p>
<b>CommonsCloud</b>, el núvol procomú, és una plataforma que integra diverses aplicacions lliures per oferir un servei de núvol digital cooperatiu, alternatiu als núvols corporatius com Google Drive i Dropbox. Estem construïnt aquesta plataforma com un projecte col·lectiu des de la cooperació, l'autogestió i el mutualisme.
<br />
<b>FemProcomuns, SCCL</b> és una cooperativa de consum i treball creada com un instrument estratègic per fer viables i sostenibles els projectes procomuns. El Grup d'Activitat Cooperativitzada CommonsCloud és el marc des d'on l'equip impulsor de la plataforma coordina el seu desenvolupament i manteniment i on les usuàries sòcies cooperativistes poden participar-hi.
<br />
<b>L'Aliança CommonsCloud</b> està formada per un conjunt d'entitats que des de la col·laboració i la intercooperació participen en la producció de la plataforma, en el seu desplegament i difusió, o que ofereixen serveis associats a aquesta.
</p>
<p>
<b>Com formar-ne part?</b>
<br />
<b>Gent CommonsCloud</b> sou les persones que us heu donat d'alta al servei; amb aquest compte podeu accedir a alguns serveis de la plataforma. <a href="/registre">Dona't d'alta</a>
<br />
<b>Gent cooperativista CommonsCloud</b> són els qui s'han associat, poden fer servir els serveis contractats i poden participar al GAC CommonsCloud com a usuàries. Per associar-se cap fer una aportació mínima de 10€ al capital social de la cooperativa FemProcomuns i escollir la quota segons els serveis que es volen tenir. Fes-te sòcia [link]
<br />
En la primera fase de llançament de la plataforma la manera d'accedir als serveis és fent una aportació a la campanya de matchfunding Conjuntament a Goteo. <a href="https://ca.goteo.org/project/commonscloud">Fes una aportació</a><br />
</p>
<p>
Contribueix al CommonsCloud, el núvol procomú. Fes el salt!
</p> colectivo All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses.  edita <p>
<b>Hola gent!</b>
</p>
<p>
Aquí pots crear un compte per al servei de núvol digital cooperatiu i procomú CommonsCloud. Oferim una alternativa als núvols corporatius integrant les millors aplicacions de programari lliure per oferir una alternativa autogestionada, ètica i respectuosa amb la privacitat.
<br />
Alguns serveis de CommonsCloud són gratuïts només donant d'alta el compte.
<br />
Per altres serveis cal que et facis sòcia cooperativista i paguis una quota, perquè estem construïnt aquesta plataforma col·lectivament des de la mutualització del servei. Et pots associar individualment o com a col·lectiu.
El "Grup d'Activitat Cooperativitzada CommonsCloud" de la cooperativa integral FemProcomuns, SCCL és el marc des d'on s'ofereix el servei i "l'Aliança CommonsCloud" coordinada des d'aquest grup és la que el produeix.
<br />
<b>En la fase de llançament la manera d'accedir als serveis cooperatius és fent una aportació a la campanya de matchfunding Conjuntament a Goteo.</b>

<ul>
<li>Dona't d'<a href="/registre">alta</a></li>
<li>Informa't de com <a href="/about">associar-te</a></li>
<li><b>Fes una aportació per formar part de CommonsCloud</b> <a href="https://ca.goteo.org/project/commonscloud">https://ca.goteo.org/project/commonscloud</a></li>
</ul>

</p> es mejor no es una buena contraseña <p>
Un sol compte per accedir a tots els serveis de CommonsCloud; Oficina, Agora i Projectes.
<br />
Alguns serveis són oberts i gratuïts, per altres caldrà que et facis sòcia cooperativista. <a href="/about">Informa't</a>
</p> servicio Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>Como encontrar ayudar</h4>
<ol>

    <li>Lee la documentación</li>

    <li>Consulta con la comunidad de usuarios</li>

    <li>Abrir una incedencia</li>

</ol>

<table>


    <tr>

    <td><b>Oficina</b></td>

    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentación</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>


    <tr>

    <td><b>Projectes</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentación</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>


    <tr>

    <td><b>Àgora</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentación</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Comunidad</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidencias</td>

    </tr>

</table>

<h4>CommonsCloud</h4>
<p>
Comparte dudas y soluciones sobre la plataforma CommonsCloud
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>No puedo hacer un login</h4>
<p>
Tu cuenta te usuario serve para logear en todos los servicios que tienes disponibles de CommonsCloud.
<br />
No hace falta tener más de una cuenta.
<br />
<br />
Si no puedes logear, <a href="https://www.commonscloud.coop/recover-password">resetea tu contraseña</a>
<br />
Te presente que tu "nombre de usuario", No es tu correo electrónico.
<br />
Si aun así no puedes hacer un login, escríbenos a suport@commonscloud.coop
<p/> 