��          �      �       H     I     i     |     �     �  
   �     �  "   �  
             $     A     a  �  n  R     R   `  \   �  �     �   �    f  �   ~  9  o  T  �  �   �    �  �   �  f  �                                            
   	                  _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective_management_explained collective_manager_group_explained index_page register_page service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-11-27 19:41+0100
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language: en
Language-Team: en <chris@commonscloud.coop>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
X-Generator: Poedit 1.8.11
 Welcome to commonsCloud.

Please confirm your email at this link
%s

Kind regards. Welcome to commonsCloud.

Please confirm your email at this link
%s

Kind regards. Hello,

There is person requesting an account for %s.

Name: %s
email: %s

%s

Kind regards. Hello %s,

Someone has requested your password to be changed.
If you did not do it, please ignore this email.

Your username is: %s

Please set your password at femProcomuns here
%s

Kind regards. Hello %s,

Your account has been created.
Your username is: %s (Remember this!)

Please set your password at femProcomuns here
%s

Kind regards. <p>
<b>CommonsCloud</b>, the pro-common cloud, is a platform the integrates various free software applications to offer a digital cooperative cloud. We are building the platform as a collective project based on cooperation and self management.
</p>
<p>
<b>FemProcomuns, SCCL</b> is a cooperative created as an strategic instrument to make pro-commons projects viable and sustainable. The <b>Grup d'Activitat Cooperativitzada CommonsCloud (GAC)</b> is the place where the core team members of the platform coordinate development and mantainence,  members of the cooperatvie can participate here too.
</p>
<p>
<b>The CommonsCloud alliance</b> is made up of entities who participate in the production of the platform, it's deployment, and communication of the product to a wider audience.
</p>

<h3>How to join us?</h3>
<p> 
<b>CommonsCloud people</b> are those who have created an account; with this account you can access some of the servies on the platform. <a href="/registre">Create an account</a>
</p>
<p>
<b>CommonsCloud coop members</b> can make use of the contracted services and can participate in the GAC as users. To join up, you need to make a minimum of €10 contribution as social capital to the cooperative FemProcomuns and chose the quota according to the service you which to enjoy.
</p> All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. <p>
We offer an alternative to the corprative cloud integrating the best applications to offer our self-governed cloud with free software, ethical and at the same time respecting privacy.
</p>
<p>
Some of the CommonsCloud services are free as soon as you create an account.
</p>
<p>
Other services require you to become a member of the cooperative and pay a fixed quota because we are building this platform collectively as a mutual service. You can associate yourself as and individual or as a collective. From the "Grup d'Activitat Cooperativitzada CommonsCloud" of the cooperative FemProcomuns, SCCL we offer the service and the 'CommonsCloud alliance".
</p>
<ul>
<li>Create an <a href="https://www.commonscloud.coop/registre">account</a></li>
<li>Find out how your can <a href="https://www.commonscloud.coop/about">associate</a> yourself</li>
</ul> <p>
Just one account to access all CommonsCloud services; Office, Agora i Projects.
<br />
Some services are open and free, others require you to become part of our cooperative. <a href="/about">Find out more</a>
</p> Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>How to find help</h4>
<ol>
    <li>Read the documentation ;)</li>
    <li>Ask the community of users</li>
    <li>Open an issue</li>
</ol>

<table>
    <tr>
    <td><b>Office</b></td>
    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentation</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
    <tr>
    <td><b>Projectes</b></td>
    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentation</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
    <tr>
    <td><b>Àgora</b></td>
    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentation</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Community</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>
    <td>Issues</td>
    </tr>
</table>

<h4>CommonsCloud</h4>
<p>
Share doubts, suggestions and solutions about the CommonsCloud platform
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>I can't login!</h4>
<p>
Your same user account is valid for all the services you have access to on the platform.
<br />
You only need one user account
<br />
<br />
If you can't login, <a href="https://www.commonscloud.coop/recover-password">reset your password</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
<br />
Remember that your username is not your email!
<br />
If you still can't login, send us an email at suport@commonscloud.coop
<p/> 