��    }        �   �      �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
          (     A     H     Z  -   a     �  
   �  #   �     �     �     �     �               !     &     2     ?     Q     W     e  
   x     �  *   �     �     �     �     �     �     �          (     1     7     >     O  
   W     b     g     t     �     �     �     �     �     �       3        M     Y     f     �     �     �  !   �     �     �     �     �          	  &        A     N     ^     f     n      v  )   �     �  7   �  (     0   ;     l     �  
   �     �     �     �     �     �     �      �       2   4     g          �  %   �     �  *   �     �       +        F     W     e     v     �     �     �     �     �  
   �       "   #     F  
   K  	   V     `     w     �     �     �  �  �     `     q     �     �     �     �     �  	   �     �                4     ;     S  3   _     �  	   �  2   �     �     �                    /     ;     A     N     \     j     }  (   �     �  #   �  .   �     $     ?     M     S     X     i  
   �     �     �     �     �     �     �     �     �     �     �          '     8     K  !   k     �  A   �     �     �  )        :     J     P  %   f     �     �     �     �     �     �  5   �     $     8     G     V     ]     e  )   �     �  7   �  2   �  9   /     i     �     �     �     �     �     �            7   *     b  4   v  "   �     �     �  -   �       =   $     b     }  -   �     �     �     �     �  m     m     k   �  �   Y   �   7!  W  �!  �   4)  9  %*     _+  �  f+  
   �.     /  �   !/    	0  �   1  �  �1     4   V       Z                 %       ^   0      !   W                     .       {   f           o      \          _   >   A   z       S          y               p   g   |          ,   F       n   )       J      5   N       K   9   (   X   }   l                  C   @   e      L      -       *          ?           $   s   U   2   d   
   c   #   k   v   3   M       B          [   G   &                  ;   R   Y      w   =   8                   D      r   t   x              q   P   m   a             u   "   ]      6      '   I   1      h       	   E      :   j   i          O   T   +   /   `           <      H   b   7   Q       (check spam) 404 Page not found About us Activate account Add member Add new member Add user Added %s Already exist in the database Already got an account and Attribute '%s' not found Avatar Avatar changed OK Cancel Cannot delete. User has been exported to LDAP Change password Collective Confirm your email at Commons Cloud Credentials needed DN not valid Delete Delete entry Delete petition Does not exist Edit Edit avatar Edit profile Edit your profile Email Email address Email confirmed OK Email sent Emails do not match Finish onboarding process at Commons Cloud Forgotten your password? Fullname Groups Hello Image formats Insufficient permission: %s Kind regards Language Login Logout Maintenance mode Members My profile Name New password New user requesting an account No file part No selected file Notify manager Onboarding complete Onboarding not yet finished Password changed OK Passwords must match Please be patient. This might take one or two days. Please edit Please login Please set your new password Preferred language Quota Recover password Recover password at Commons Cloud Registre Repeat email address Repeat password Save Search Send email again Send the user an email to set password Set password Short biography Sign up Support Surname Tell us something about yourself Thank you for signing up at Commons Cloud The new username will be The whole process shouldn't take more than a day or two There were fleas last summer in Menorca. Think of a longer, easier to remember passphrase This can't be undone Unable to update Updated OK User deleted User not found User not found: %s User petition Username Valid credentials needed Waiting for user to set password We create your account We sent you an email to comfirm your email address We've sent you an email Websites Welcome You can manage your user profile here You may login here You receive an e-mail to set your password Your account is activated Your groups Your password has been changed successfully Your password is Your username Your username is Your websites _confirmEmailAddress_email_text _confirmEmail_text _newUserWaiting_email_text _resetPassword_text _setPassword_text about_page collective_management_explained collective_manager_group_explained edit index_page is better is not a good password register_page service_management_explained service_manager_group_explained support_page Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-11-27 19:41+0100
PO-Revision-Date: 2018-11-27 23:22+0100
Last-Translator: 
Language: ca
Language-Team: ca <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
X-Generator: Poedit 1.8.11
 (comprovar spam) 404 pàgina no trobada Sobre nosaltres Activar compte Afegir sòcia Afegir nova sòcia Afegir usuari Afegit %s Ja existeix en la base de dades Ja tens un compte d'usuari i Atribut '%s' no trobat Avatar Canvi d'avatar correcte Cancel·lar No es pot eliminar. Uusari ha sigut exportat a LDAP Canviar contrasenya Collectiu Confirma el teu correu electrònic a Commons Cloud Falten credencials DN invàlid Eliminar Eliminar entrada Eliminar petició No existeix Edita Edita avatar Editar perfil El teu perfil Correu electrònic Adreça de correu electrònic Confirmació correu electrònic correcte Correu envat Correus electrònic no coincideixen Acaba el procés d'embarcament a Commons Cloud Has perdut la contrasenya? Nom i cognoms Grups Hola Formats d'imatge Permisos insuficients: %s Fins aviat Idioma Entra Sortir Mode de manteniment Sòcies El meu perfil Nom Contrasenya nova Usuari nou demana un compte Cap part de fitxer Cap fitxer seleccionat Notificar gestor Embarcament acabat Embarcament encara no ha acabat Contrasenya canviada correctament Contrasenyes han de coincidir Et demanem paciència. Aquest pas pot trigar entre un i dos dies. Si us plau edita Si us plau, entra Si us plau, estableix la nova contrasenya Idioma preferit Quota Recuperar contrasenya Recuperar contrasenya a Commons Cloud Registre Repeteix l'adreça de correu Repeteix contrasenya Guarda Cercar Torna a enviar el correu Envia a l'usuari un correu per definir la contrasenya Posa la contrasenya Breu biografia Crea un compte Suport Cognoms Explica'ns una cosa sobre tu Gràcies per registrat-te a Commons Cloud El nou nom d'usuari és Tot el procés no hauria de trigar més d'un o dos dies Fa dos estius hi havia puces a l'hotel Menorquina. Pensa en una contrasenya més llarga i fàcil de recordar Això no es pot desfer Actualització no és possible Actualitzat correctament Usuari eliminat Usuari no trobat Usuari no trobat: %s Petició d'usuària Nom d'usuari Falten credencials vàlids Esperant que la persona usuària canviï la contrasenya Creem el teu compte T'hem enviat un correu per confirmar la teva adreça T'hem enviat un correu electrònic Pàgines Benvingut/da Pots gestionar el teu perfil d'usuària aquí Pots entrar aquí Rebràs un correu electrònic per canviar la teva contrasenya El teu compte s'ha activat Els teus grups La teva contrasenya s'ha canviat correctament La teva contrasenya és El teu nom d'usuari El teu nom d'usuari és Les teves pàgines Benvingut/da a commonsCloud.

Si us plau, confirma la teva adreça de correu en aquest enllaç
%s

Fins aviat Benvingut/da a commonsCloud.

Si us plau, confirma la teva adreça de correu en aquest enllaç
%s

Fins aviat Hola,

Hi ha una persona que ha demanat un compte per a %s.

Nom: %s
Correu electrònic: %s

%s

Fins aviat Hola %s,

Una persona ha demanat canviar la seva contrasenya.
Si no l'has demanat tu, si us plau ignora aquest correu.

El teu nom d'usuari és: %s

Pots canviar tu la teva contrasenya de commonsCloud aquí
%s

Fins aviat Hola %s,

S'ha creat el teu compte d'usuari a commonsCloud.
El teu nom d'usuari és: %s (Recorda'l!)

Si us plau, estableix la teva contrasenya aquí
%s

Fins aviat <p>
<b>CommonsCloud</b>, el núvol procomú, és una plataforma que integra diverses aplicacions lliures per oferir un servei de núvol digital cooperatiu, alternatiu als núvols corporatius com Google Drive i Dropbox. Estem construint aquesta plataforma com un projecte col·lectiu des de la cooperació, l'autogestió i el mutualisme.
<br />
<b>FemProcomuns, SCCL</b> és una cooperativa de consum i treball creada com un instrument estratègic per fer viables i sostenibles els projectes procomuns. El Grup d'Activitat Cooperativitzada CommonsCloud és el marc des d'on l'equip impulsor de la plataforma coordina el seu desenvolupament i manteniment i on les usuàries sòcies cooperativistes poden participar-hi.
<br />
<b>L'Aliança CommonsCloud</b> està formada per un conjunt d'entitats que des de la col·laboració i la intercooperació participen en la producció de la plataforma, en el seu desplegament i difusió, o que ofereixen serveis associats a aquesta.
</p>
<p>
<b>Com formar-ne part?</b>
<br />
<b>Gent CommonsCloud</b> sou les persones que us heu donat d'alta al servei; amb aquest compte podeu accedir a alguns serveis de la plataforma. <a href="/registre">Dóna't d'alta</a>
<br />
<b>Gent cooperativista CommonsCloud</b> són els qui s'han associat, poden fer servir els serveis contractats i poden participar al GAC CommonsCloud com a usuàries. Per associar-se cap fer una aportació mínima de 10€ al capital social de la cooperativa FemProcomuns i escollir la quota segons els serveis que es volen tenir. Fes-te sòcia [link]
<br />
En la primera fase de llançament de la plataforma la manera d'accedir als serveis és fent una aportació a la campanya de matchfunding Conjuntament a Goteo. <a href="https://ca.goteo.org/project/commonscloud">Fes una aportació</a><br />
</p>
<p>
Contribueix al CommonsCloud, el núvol procomú. Fes el salt!
</p> All Commons Cloud users are grouped together in 'Collectives'.
<br />
You manage members of the collective and can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. Members of this group are <b>Collective Managers</b> and will <b>receive an email notification</b> when an anonymous person requests an account as a collective member.
<br />
Managers can 1) Create new user accounts for people who belong to the collective, 2) Establish disk quotas, and 3) Change email addresses. editar <p>
Oferim una alternativa als núvols corporatius integrant les millors aplicacions per oferir el nostre núvol autogestionat amb programari lliure, ètica i respectuosa amb la privacitat.
</p>
<p>
Alguns serveis de CommonsCloud són gratuïts només donant d'alta el compte.
</p>
<p>
Per altres serveis cal que et facis sòcia cooperativista i paguis una quota, perquè estem construïnt aquesta plataforma col·lectivament des de la mutualització del servei. Et pots associar individualment o com a col·lectiu. El "Grup d'Activitat Cooperativitzada CommonsCloud" de la cooperativa integral FemProcomuns, SCCL és el marc des d'on s'ofereix el servei i "l'Aliança CommonsCloud" coordinada des d'aquest grup és la que el produeix.
</p>
<ul>
<li>Dona't d'<a href="https://www.commonscloud.coop/registre">alta</a></li>
<li>Informa't de com <a href="https://www.commonscloud.coop/about">associar-te</a></li>
</ul> és millor no és una bona contrasenya <p>
Un sol compte per accedir a tots els serveis de CommonsCloud; Oficina, Agora i Projectes.
<br />
Alguns serveis són oberts i gratuïts, per altres caldrà que et facis sòcia cooperativista. <a href="/about">Informa't</a>
</p> Services normally refer to websites, however a service is in fact any Commons Cloud service that requires a user to login.
<br />
You can add or remove <b>any user with a Commons Cloud account</b> to the a service you manage, meaning <b>you decide who can login or not</b>. Members of this group are <b>Service Managers</b> and can add or remove any user with a Commons Cloud account, meaning they <b>decide who can login or not</b>. <h4>Com trobar ajuda</h4>
<ol>

    <li>Llegeix la documentació ;)</li>

    <li>Consulta con la comunitat d'usuaris</li>

    <li>Obre una incidència</li>

</ol>

<table>


    <tr>

    <td><b>Oficina</b></td>

    <td><a target="_blank" href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/oficina/">Documentación</a> <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-loficina">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>


    <tr>

    <td><b>Projectes</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/projectes/">Documentació</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/us-de-projectes">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>


    <tr>

    <td><b>Àgora</b></td>

    <td><a target="_blank"  href="https://projectes.commonscloud.coop/w/commonscloud/docs/usuari/agora/">Documentació</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>


    <td><a target="_blank"  href="https://agora.commonscloud.coop/c/site-feedback">Comunitat</a>  <i class="fa fa-external-link" aria-hidden="true"></i></td>

    <td>Incidències</td>

    </tr>

</table>

<h4>Comunidad</h4>
<p>
Comparteix dubtes i solucions sobre la plataforma CommonsCloud
<br />
<a target="_blank"  href="https://agora.commonscloud.coop/c/commonscloud">https://agora.commonscloud.coop/c/commonscloud</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
</p>

<h4>No puc entrar-hi</h4>
<p>
El teu compte d'usuària serveix per entrar a tots els serveis que tens a la teva disponibilitat a CommonsCloud.
<br />
Només cal un compte d'usuària.
<br />
<br />
Si no pots entrar-hi, <a href="https://www.commonscloud.coop/recover-password">demana una nova contrasenya</a>  <i class="fa fa-external-link" aria-hidden="true"></i>
<br />
Recorda que el teu "nom d'usuària" no es el teu correu electrònic!
<br />
Si encara no has pogut entrar, envia'ns un coreu a suport@commonscloud.coop
<p/> 