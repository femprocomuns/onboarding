# -*- coding: utf-8 -*-
from onboarding import app
import string, os, re
from random import choice
from flask import session, request
import models
from unidecode import unidecode
import smtplib
from email.MIMEText import MIMEText
from flask_babel import gettext, force_locale
from flask import flash
from sqlalchemy import or_, and_
from ldap_helper import *
from models import User

def buildMenu():
    menu = []
    if session and session.has_key('username'):
        menu.append((gettext('My profile'), '/profile'))
    else:
        menu.append((gettext('Registre'), '/registre'))
    menu.append((gettext('Support'), '/support'))
    menu.append((gettext('Contribute'), 'https://agora.commonscloud.coop/t/contribucions-al-commonscloud/92'))   
    
    if session and session.has_key('manager') and session['manager'] != False: 
        menu.append((gettext('Users'), '/manage-users'))
        if session.has_key('managed_collectives') and session['managed_collectives']:
            menu.append((gettext('Groups'), '/manage-groups'))
        if session.has_key('managed_services') and session['managed_services']:
            menu.append((gettext('Services'), '/manage-services'))

    if session and session.has_key('username'):
        menu.append((gettext('Logout'), '/logout'))
    else:
        menu.append((gettext('Login'), '/login'))
    return menu


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in app.config['PHOTO_EXTENSIONS']

def getUniqueToken(length=64):
    token = generateRandomPassword(length)
    while User.query.filter_by(token=token).first():
        token = generateRandomPassword(length)
    return token
    
def generateRandomPassword(length=16):
    characters = string.ascii_letters + string.digits
    return "".join(choice(characters) for x in range(length))

def sanitizeUID(uid):
    uid = unidecode(uid)
    uid = ''.join(uid.lower().split())
    uid = uid.replace(" ", "")
    return re.sub('[^A-Za-z0-9\-]+', '', uid) # also escaping uid

def sanitizeGroupRDN(rdn):
    rdn = unidecode(rdn)
    rdn = ''.join(rdn.split())
    # TODO first letter or word uppercase
    rdn = rdn.replace(" ", "")
    return re.sub('[^A-Za-z0-9\-]+', '', rdn) # also escaping rdn    
    
    
def checkUsername(uid):
    return False
    
def isAdmin():
    if 'admin' in session and session['admin'] == True:
        return True
    return False

def isManager():
    if 'managed_collectives' in session and session['managed_collectives']:
        return True
    if isAdmin():
        return True
    return False

def canViewUser(con, dn):
    if not isValidDN(dn):
        return False
    if session and session['username'] and session['username'] == dn:
        return True
    if isAdmin():
        return True
    if canManageCollective(dn):
        return True
    if getCommonMembership(con, dn):
        return True
    return False

def canEdit(dn):
    if not isValidDN(dn):
        return False
    if isAdmin():
        return True
    if session and session['username'] and session['username'] == dn:
        return True
    return False

def canManageUser(primary_collective_dn):
    if isAdmin():
        return True
    return canManageCollective(primary_collective_dn)

def canViewGroup(groupInfo):
    if isAdmin():
        return True
    if canManageGroup(groupInfo):
        return True
    group_dn = groupInfo['dn']
    if isCollective(group_dn):
        if group_dn in session['collectives'].keys() or group_dn in session['managed_collectives']:
            return True
    if isService(group_dn):
        if group_dn in session['services'].keys() or group_dn in session['managed_services']:
            return True
        
    if groupInfo['isManagerGroup']:
        managed_dn = groupInfo['managedGroup']['dn']
        if managed_dn in session['collectives'].keys() or managed_dn in session['services'].keys():
            # Users can see the "groups of managers" of the groups they are members of
            return True
        if managed_dn in session['managed_collectives'] or managed_dn in session['managed_services']:
            # Managers can see the "groups of managers" of the groups they manage
            return True
    return False

def canManageGroup(groupInfo):
    if isAdmin():
        return True
    group_dn = groupInfo['dn']
    if isService(group_dn) and canManageService(group_dn):
        return True
    if isCollective(group_dn) and canManageCollective(group_dn):
        return True
    if isCollectiveSubGroup(group_dn):
        collective_dn = group_dn.split(',',1)[1]
        if canManageCollective(collective_dn):
            return True
    if groupInfo['isManagerGroup'] and session['username'] in groupInfo['attributes']['member']:
        return True
    return False

def canManageService(service_dn):
    if not isService(service_dn):
        return False
    if service_dn in session['managed_services']:
        return True
    return False

def canManageCollective(collective_dn):
    if not isCollective(collective_dn):
        return False
    if collective_dn in session['managed_collectives']:
        return True
    return False

def getGroupInfo(groupOfNames):
    dn = groupOfNames.get_dn()
    attributes = groupOfNames.get_attributes()
    result = {}
    result['dn']=dn
    result['groupName'] = groupOfNames.get_rdn()
    result['attributes']=attributes
    result['isService']=False
    result['isCollective']=False
    result['isCollectiveSubGroup']=False
    result['isManagerGroup']=False
    result['managesService']=False
    result['managesCollective']=False
    result['managedGroup']={}
    result['managingGroup']={}
    result['hasManagingGroup']=False
    result['parentDN']=getParentDN(dn)
    if isService(dn):
        result['isService']=True
        result['hasManagingGroup']=True
        managingGroup_dn=attributes['owner'][0]
        result['managingGroup']['dn'] = managingGroup_dn
        result['managingGroup']['rdn'] = getRDN(managingGroup_dn)
        return result
    if isCollective(dn):
        result['isCollective']=True
        result['hasManagingGroup']=True
        managingGroup_dn=attributes['owner'][0]
        result['managingGroup']['dn'] = managingGroup_dn
        result['managingGroup']['rdn'] = getRDN(managingGroup_dn)
        return result
    if isManagerGroup(dn):
        result['isManagerGroup']=True
        managedGroup_dn=attributes['owner'][0]
        result['managedGroup']['dn'] = managedGroup_dn
        result['managedGroup']['rdn'] = getRDN(managedGroup_dn)
        if isService(managedGroup_dn):
            result['managesService']=True
            return result
        if isCollective(managedGroup_dn):
            result['managesCollective']=True
            return result
    if isCollectiveSubGroup(dn):
        result['isCollectiveSubGroup'] = True
        return result
    return result

def getGroupType(group_dn):
    if isService(group_dn):
        return 'service'
    if isCollective(group_dn):
        return 'collective'
    if isCollectiveSubGroup(group_dn):
        return 'collectiveSubGroup'
    return None

def isService(group_dn):
    if not isValidDN(group_dn):
        return False
    if group_dn.split(',',1)[1] == app.config['SERVICE_OU_DN']:
        return True
    return False

def isCollective(group_dn):
    if not isValidDN(group_dn):
        return False
    if group_dn.split(',',1)[1] == app.config["COLLECTIVE_OU_DN"]:
        return True
    return False

def isCollectiveSubGroup(group_dn):
    if not isValidDN(group_dn):
        return False
    if isManagerGroup(group_dn):
        return False
    try:
        if group_dn.split(',',2)[2] == app.config["COLLECTIVE_OU_DN"]:
            return True
    except:
        return False
    return False

def isManagerGroup(group_dn):
    if not isValidDN(group_dn):
        return False
    ou = group_dn.split(',',1)[1]
    if ou == app.config["COLLECTIVE_MANAGER_OU_DN"] or ou == app.config["SERVICE_MANAGER_OU_DN"]:
        return True
    return False

def getCollectiveRDN(dn):
    if not isValidDN(dn):
        return None
    if isCollective(dn):
        return dn[len('cn='):-len(app.config["COLLECTIVE_OU_DN"])-1]
    return None

def getRDN(dn):
    pos1 = dn.find('=')+1
    pos2 = dn.find(',')
    return dn[pos1:pos2]

def getParentDN(dn):
    i = dn.find(',')+1
    return dn[i:]

def getUID(dn):
    if not isValidDN(dn):
        return None
    if dn.startswith('uid=') and dn.endswith(app.config['USER_OU_DN']):
        return dn[len('uid='):-len(app.config['USER_OU_DN'])-1]
    return None

def getUserDN(uid):
    userDN = "uid=%s,%s" % (uid, app.config['USER_OU_DN'])
    if not isValidDN(userDN):
        return None
    return userDN

def getCollectiveDN(collective_name):
    collectiveDN = "cn=%s,%s" % (collective_name, app.config['COLLECTIVE_OU_DN'])
    if not isValidDN(collectiveDN):
        return None
    return collectiveDN

def doesCollectiveDNExistInDIT(con, dn):
    try:
        collective = con.search_s(dn, ldap.SCOPE_BASE, '(&(objectclass=groupOfNames))')
        return collective
    except:
        return False



########## SQL

def findExportedUser(word):
    query = User.query
    columns = ['uid', 'name', 'surname', 'mail', 'primary_collective']
    query = query.filter(or_(*[getattr(User, column).ilike('%'+word+'%') for column in columns]))
    query = query.filter(and_(User.exported != None))

    return query.order_by(User.uid).all()
    
    
def filter_query(query):
    filtered = False
    
    # admin can search for users belonging to all categories
    # else only search in session['managed_collectives']
    if not session['admin']:
        collectives = [getRDN(i) for i in session['managed_collectives']]
        query = query.filter(User.primary_collective.in_(collectives))
    
    args = request.args.to_dict()
    for arg in args:
        if arg == 'word' and args[arg]:
            columns = ['uid', 'name', 'surname', 'mail', 'primary_collective']
            value = args[arg]
            filtered = True
            # query all columns for value
            query = query.filter(or_(*[getattr(User, column).ilike('%'+value+'%') for column in columns]))
        if arg == 'state' and args[arg]:
            value = args[arg]
            if value == 'unattended':
                query = query.filter(and_(User.exported == None))
            if value == 'exported':
                query = query.filter(and_(User.exported != None, User.activation_email_sent == None))
            if value == 'email-sent':
                query = query.filter(and_(User.activation_email_sent != None, User.activated == False))
            if value == 'active':
                query = query.filter(and_(User.activated == True))  
                          
    return query, filtered


########### LDAP

# dictionary of (web)services this user can login to.
def getWebSites(con, dn):
    websites = []
    if not isValidDN(dn):
        return []
    results = con.search_s( app.config['BASE_DN'], ldap.SCOPE_SUBTREE, '(&(objectClass=groupOfNames)(member=%s))' % dn )
    results = get_search_results(results)
    for result in results:
        if 'businessCategory' in result.get_attr_names():
            websites.append(result.get_attr_values('businessCategory')[0])
    return app.config['DEFAULT_WEBSITES'] + websites

# dictionary of (web)services the logged in user shares with the queried user(dn).
def getCommonWebSites(con, dn):
    if not isValidDN(dn):
        return []    
    websites_1 = getWebSites(con, dn)
    websites_2 = getWebSites(con, session['username'])
    result = []
    for url in websites_1:
        if url in websites_2:
            result.append(url)
    return result
    

# collectives this user is a memberOf
def getCollectiveMembership(con, dn):
    if not isValidDN(dn):
        return {}
    result = {}
    membership = con.search_s( app.config['COLLECTIVE_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectClass=groupOfNames)(member=%s))' % dn )
    membership = get_search_results(membership)
    for group in membership:
        result[group.get_dn()] = {'name': group.get_attr_values('cn')[0], 'description': group.get_attr_values('description')[0] }
    return result  


# services this user is a memberOf
def getServiceMembership(con, dn):
    if not isValidDN(dn):
        return {}
    result = {}
    membership = con.search_s( app.config['SERVICE_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectClass=groupOfNames)(member=%s))' % dn )
    membership = get_search_results(membership)
    for group in membership:
        result[group.get_dn()] = {'name': group.get_attr_values('cn')[0], 'description': group.get_attr_values('description')[0] }
    return result  


"""
The Collectives the dn shares with the logged in user
"""
def getCommonMembership(con, dn):
    if not isValidDN(dn):
        return {}
    groups_1 = getCollectiveMembership(con, dn)
    groups_2 = getCollectiveMembership(con, session['username'])
    result = {}
    for group_dn, value in groups_1.iteritems():
        if group_dn in groups_2:
            result[group_dn] = value
    return result   


# list of collective names
def getAllCollectivesNames(con):
    dn = app.config['COLLECTIVE_OU_DN']
    groupOfNames = con.search_s( dn, ldap.SCOPE_ONELEVEL, '(objectClass=groupOfNames)' )
    groupOfNames = get_search_results(groupOfNames)
    collectives = []
    for group in groupOfNames:
        collectives.append(group.get_attr_values('cn')[0])
    return collectives

# collectives this user can manage
def getManagedCollectiveDNs(con, dn):
    if not isValidDN(dn):
        return None
    collectives = []     
    if session['admin'] == True:
        # admin can manage all collectives
        groupsOfNames = con.search_s(app.config['COLLECTIVE_MANAGER_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectclass=groupOfNames))' )
    else:
        groupsOfNames = con.search_s(app.config['COLLECTIVE_MANAGER_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectclass=groupOfNames)(member=%s))' % dn )

    if groupsOfNames:
        # These collectives are 'managed'
        groupsOfNames = get_search_results(groupsOfNames)
        for group in groupsOfNames:
            if 'owner' in group.get_attributes():   # maybe the group that is managed (aka owner) doesn't exist
                # owner is the dn of the collective that is managed
                collectives.append(group.get_attr_values('owner')[0].encode('utf-8'))
    return collectives


# services this user can provide access to
def getManagedServiceDNs(con, dn):
    if not isValidDN(dn):
        return None
    services = []     
    if session['admin'] == True:
        # admin can manage all collectives
        groupsOfNames = con.search_s(app.config['SERVICE_MANAGER_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectclass=groupOfNames))' )
    else:
        groupsOfNames = con.search_s(app.config['SERVICE_MANAGER_OU_DN'], ldap.SCOPE_ONELEVEL, '(&(objectclass=groupOfNames)(member=%s))' % dn )

    if groupsOfNames:
        # These collectives are 'managed'
        groupsOfNames = get_search_results(groupsOfNames)
        for group in groupsOfNames:
            # owner is the dn of the service that is managed
            if 'owner' in group.get_attributes():   # maybe the group that is managed (aka owner) doesn't exist
                services.append(group.get_attr_values('owner')[0].encode('utf-8'))
    return services


def populateSessionPermissions(con):
    if con.search_s( session['username'], ldap.SCOPE_BASE, '(&(objectclass=*)(memberof=%s))' % app.config['ADMIN_GROUP'] ):
        session['admin'] = True
    else:
        session['admin'] = False
    session['collectives'] = getCollectiveMembership(con, session['username'])    # a Dict of collectives this user belongs to
    session['services'] = getServiceMembership(con, session['username'])    # a Dict of services this user belongs to
              
    session['managed_collectives'] = getManagedCollectiveDNs(con, session['username'])
    session['managed_services'] = getManagedServiceDNs(con, session['username'])
    if session['managed_services'] or session['managed_collectives']:
        session['manager'] = True
    else:
        session['manager'] = False

def getCollectiveManagersEmails(con, primary_collective):
    managers_emails = []    
    collectiveDN = "cn=%s,%s" % (primary_collective, app.config['COLLECTIVE_OU_DN'])

    try:
        collective = con.search_s( collectiveDN, ldap.SCOPE_BASE, '(objectClass=groupOfNames)' )
    except:
        return managers_emails

    attribs = get_search_results(collective)[0]
    if attribs.has_attribute('owner'):
        managerGroupDN = attribs.get_attr_values('owner')[0]
        try:
            managers = con.search_s(managerGroupDN, ldap.SCOPE_ONELEVEL, '(&(objectclass=groupOfNames))' )
        except:
            return managers_emails

        manager = con.search_s(attribs.get_attr_values('owner')[0], ldap.SCOPE_BASE)
        attribs = get_search_results(manager)[0]
        for memberDN in attribs.get_attr_values('member'):
            mail = getUserEmail(con ,memberDN)
            if mail:
                managers_emails.append(mail)
    return managers_emails


def getUserEmail(con, dn):
    user = User.query.filter_by(dn=dn).first()
    if user:
        return user.mail
    if app.config['USER_OU_DN'] in dn:
        try:
            user = con.search_s(dn, ldap.SCOPE_BASE, '(&(objectClass=inetOrgPerson))')
        except:
            return
        if user:
            attribs = get_search_results(user)[0]
            return attribs.get_attr_values('mail')[0]
    return


"""
# who is the manager of the groupOfNames
def getCollectiveOwner():
    con = bind(app.config['NOBODY'], app.config['NOBODY_PASSWD'])
    collective = con.search_s( dn, ldap.SCOPE_BASE)
    collective = get_search_results(collective)
    owner = group.get_attr_values('owner')[0]
"""
 

########### SMTP
# Send this when we don't know if the email is valid
def sendConfirmEmail(user, request):
    with force_locale(user.language):
        subject = gettext("Confirm your email at Commons Cloud")
        link = '%sconfirm-email/%s' % (request.url_root, user.token)
        text = gettext("_confirmEmail_text")
        text = gettext("_confirmEmailAddress_email_text")
        text = text % link
        _sendEmail([user.mail], None, subject, text)


def sendNewUserNotification(managers_emails, user):
    username = u"%s %s" % (user.name, user.surname)
    subject = gettext("New user requesting an account")
    link = "%s/add-user/%s" % (app.config['THIS_URL'], user.id)
    text = gettext("_newUserWaiting_email_text")
    text = text % (user.primary_collective, username, user.mail, link)
    if managers_emails:
        _sendEmail(managers_emails, app.config['NOTIFICATION_EMAILS'], subject, text)
    else:
        text = text + "\npd. No collective managers to notify. Admin intervention required." 
        _sendEmail(app.config['NOTIFICATION_EMAILS'], None, subject, text)


def sendGroupEmail(bcc, subject, body):
    bcc = list(set(bcc + app.config['NOTIFICATION_EMAILS']))
    max_batch = app.config['MAX_BCC_BATCH']
    batches = [bcc[max_batch*i:max_batch*(i+1)] for i in range(len(bcc)/max_batch + 1)]
    for batch in batches:
        _sendEmail([], batch, subject, body)


# Send this when we want the user to set their passwd
def sendSetPassword(user, request):
    if user.activated:
        with force_locale(user.language):
            subject = gettext("Recover password at Commons Cloud")
            link = '%sreset-password/%s' % (request.url_root, user.token)
            text = gettext("_resetPassword_text")
            text = text % (user.name, user.uid, link)
            _sendEmail([user.mail], None, subject, text)
    else:
        with force_locale(user.language):
            subject = gettext("Finish onboarding process at Commons Cloud")
            link = '%sset-password/%s' % (request.url_root, user.token)
            text = gettext("_setPassword_text")
            text = text % (user.name, user.uid, link)
            _sendEmail([user.mail], None, subject, text)


def _sendEmail(TO, BCC, subject, text):
    msg = MIMEText(text.encode('utf-8'), "plain", "utf-8")
    msg['From'] = app.config['NO_REPLY']
    if BCC:
        msg['Bcc'] = ", ".join(BCC)
    msg['To'] = ", ".join(TO)
    msg['Subject'] = subject

    message = u"""\
From: %s
To: %s
Subject: %s

%s
""" % (app.config['NO_REPLY'], ", ".join(TO), subject, text)

    emails = TO
    if BCC:
        emails = TO + BCC

    if app.config['SENDMAIL'] == True:
        s = smtplib.SMTP('localhost')
        s.sendmail(app.config['NO_REPLY'], emails, msg.as_string())
        s.quit()
    else:
        print message
