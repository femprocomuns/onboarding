# -*- coding: utf-8 -*-
from flask import Flask
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel

csrf = CSRFProtect()
login_manager = LoginManager()

app = Flask(__name__)
app.config.from_pyfile('config.cfg')
app.config['PHOTO_EXTENSIONS'] = ['png', 'jpg', 'jpeg']

app.secret_key = app.config['SECRET_KEY']
db = SQLAlchemy(app)

csrf.init_app(app)
babel = Babel(app)

login_manager.init_app(app)

from onboarding import views

if __name__ == '__main__':
    app.run()
